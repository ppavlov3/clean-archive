import datetime
import os
from sys import argv
import shutil
import tempfile


help_msg = """
Usage: clean_app /path/to/file
"""


def app():

    cleaned_folders = []
    clear_log = []

    if len(argv) == 1 or len(argv) > 2:
        print(help_msg)
        return
    else:
        zip_file_name = argv[1]
        file_name = zip_file_name.rsplit('.', 1)[0]
    if not os.path.isfile(zip_file_name):
        print("File not exist")
        return
    else:
        is_root = False
        with tempfile.TemporaryDirectory() as tmpdir:
            clear_log.append(f'{datetime.datetime.now()} Create {tmpdir}')
            shutil.unpack_archive(zip_file_name, tmpdir)
            clear_log.append(f'{datetime.datetime.now()} Unpack {zip_file_name}')
            for dirpath, dirnames, files in os.walk(tmpdir):
                if (not is_root and len(dirnames) == 1 and len(files) == 0):
                    pass
                else:
                    if not is_root:
                        work_dir = dirpath
                        clear_log.append(f'{datetime.datetime.now()} Set work dir {tmpdir}')
                        is_root = True
                    else:
                        if '__init__.py' in os.listdir(dirpath):
                            pass
                        else:
                            relative_path = os.path.relpath(dirpath, work_dir)
                            cleaned_folders.append(relative_path)
                            shutil.rmtree(dirpath)
                            clear_log.append(f'{datetime.datetime.now()} Remove {relative_path}')

            cleaned_folders.sort()
            with open(f'{tmpdir}/cleaned.txt', 'w') as file:
                for line in cleaned_folders:
                    file.write(f"{line}\n")
                file.close
                clear_log.append(f'{datetime.datetime.now()} Create file cleaned.txt')

            shutil.make_archive(f'{file_name}_new', 'zip', tmpdir)

    clear_log.append(f'{datetime.datetime.now()} Delete {tmpdir}')

    with open('logging', 'w') as file:
        for line in clear_log:
            file.write(f"{line}\n")
        file.close


if __name__ == "__main__":
    app()
